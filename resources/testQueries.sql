USE ejemploexpense;
INSERT INTO user (lastest_login)
VALUES('2000/01/4');

INSERT INTO user_info(first_name,middle_name,last_name,recovery_email)
VALUES('carlos','fernando','arellano casale','casareafer2908@gmail.com');

INSERT INTO user (creation_date,lastest_login)
VALUES('2000/01/03','2000/01/03');

show columns from transactions;

ALTER TABLE transactions MODIFY creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE transactions MODIFY creation_date DATETIME NOT NULL;

UPDATE transactions SET 
	amount = 333,
    description = "cagastestodo",
    transaction_date = "2000/01/01",
    category_id = 1,
    account_id = 1,
    latest_update = "2020/01/29"
WHERE user_id = 1 AND id = 1;


START TRANSACTION;
INSERT INTO userexejmplazo (lastest_login)
VALUES('2000/01/01');
INSERT INTO user_info_exemplaxo(first_name,middle_name,last_name,recovery_email,user_id)
VALUES('Gerardo','Ruben','arellano casale','yayo@gmail.com',LAST_INSERT_ID());
COMMIT;


SELECT LAST_INSERT_ID();

SELECT
	First_name,
    id AS 'user ID',
    creation_date AS 'user created on'
FROM userexejmplazo
JOIN user_info_exemplaxo
	ON userexejmplazo.id = user_info_exemplaxo.user_id;

use ejemploexpense;
select * from user_info_exemplaxo;
    
drop table user_info;

DELETE FROM user WHERE id = 4;

SELECT * FROM users;

DESC login_logs;