DROP DATABASE IF EXISTS ejemploexpense;
CREATE DATABASE ejemploexpense;


CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    uuid VARCHAR(50) NOT NULL,
	creation_date DATETIME NOT NULL,
	PRIMARY KEY(id),
	UNIQUE KEY(uuid)
);

CREATE TABLE login_logs(
    id INT NOT NULL AUTO_INCREMENT,
	os VARCHAR(20),
    latest_login DATETIME NOT NULL,
    user_id INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE categories (
    id INT AUTO_INCREMENT,
    name VARCHAR(20) DEFAULT 'Max 20 letters',
    description VARCHAR(20) DEFAULT 'Max 20 letters',
    icon_url VARCHAR(100),
    user_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE accounts(
	id INT AUTO_INCREMENT,
    name VARCHAR(20) DEFAULT 'Max 20 letters',
    description VARCHAR(20) DEFAULT 'Max 20 letters',
	amount DECIMAL(20,2) NOT NULL DEFAULT '0',
	description VARCHAR(20) DEFAULT 'Max 20 letters',
	transaction_date DATETIME NOT NULL,
	category_id INT NOT NULL,
	account_id INT NOT NULL,
	user_id INT NOT NULL,
	creation_date DATETIME NOT NULL,
    amount DECIMAL(20,2) DEFAULT 0,
    icon_url VARCHAR(100),
    user_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE transactions(
	id INT NOT NULL AUTO_INCREMENT,
	latest_update DATETIME NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(category_id) REFERENCES categories(id),
	FOREIGN KEY(account_id) REFERENCES accounts(id),
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);




---------------------------------



USE ejemploexpense;

CREATE TABLE userexejmplazo(
	id INT NOT NULL AUTO_INCREMENT,
	creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    lastest_login DATETIME,
	PRIMARY KEY(id)
);

INSERT INTO userexejmplazo(lastest_login) values (current_timestamp());

SELECT * from userexejmplazo;




CREATE TABLE user_info_exemplaxo(
	first_name VARCHAR(30) DEFAULT 'Name',
    middle_name VARCHAR(45) DEFAULT 'Middle name',
    last_name VARCHAR(45) DEFAULT 'Last name',
    recovery_email VARCHAR(45) DEFAULT 'Optional',
	user_id INT NOT NULL,
    FOREIGN KEY(user_id)
    REFERENCES userexejmplazo(id)
    ON DELETE CASCADE
);

CREATE TABLE expense_category (
	id INT AUTO_INCREMENT,
    category VARCHAR(20) DEFAULT 'Max 20 letters',
    icon_url VARCHAR(100),
    user_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id)
    REFERENCES user(id)
    ON DELETE CASCADE
);

CREATE TABLE accounts(
	id INT AUTO_INCREMENT,
    account VARCHAR(20) DEFAULT 'Max 20 letters',
    icon_url VARCHAR(100),
    account_amount DECIMAL(20,2) DEFAULT 0,
    user_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id)
    REFERENCES user(id)
    ON DELETE CASCADE
);

CREATE TABLE transactions(
	id INT NOT NULL AUTO_INCREMENT,
	amount DECIMAL(20,2) NOT NULL DEFAULT '0',
	category_id INT NOT NULL,
	account_id INT NOT NULL,
	user_id INT NOT NULL,
	creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	transaction_date DATETIME NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(category_id)
	REFERENCES expense_category(id),
	FOREIGN KEY(account_id)
	REFERENCES accounts(id),
	FOREIGN KEY (user_id)
	REFERENCES user(id)
	ON DELETE CASCADE
);



