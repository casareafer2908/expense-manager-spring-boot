package com.example.demo.integration

import com.example.demo.controller.request.CreateLogRequest
import com.example.demo.entity.LoginLogs
import com.example.demo.entity.User
import com.example.demo.testutils.postExchange
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate


internal class LogInLogControllerTest : BaseIntegrationTest() {

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Test
    fun `Create a log MUST with Android os should be created correctly`() {
        for (userAndLogId in 1L..10L) {
            val uuid = "uuidx$userAndLogId"
            restTemplate.postExchange("/user/create", null, testHeaders(uuid), User::class.java)

            val resBody = restTemplate.postExchange(
                    "/login/log", CreateLogRequest("Android"), testHeaders(uuid), LoginLogs::class.java).body!!

            assertThat(resBody.id).isEqualTo(userAndLogId)
            assertThat(resBody.userId).isEqualTo(userAndLogId)
            assertThat(resBody.os).isEqualTo("android")
        }
    }

    @Test
    fun `Create a log MUST with iOS os should be created correctly`() {
        for (userAndLogId in 1L..10L) {
            val uuid = "uuidx$userAndLogId"
            restTemplate.postExchange("/user/create", null, testHeaders(uuid), User::class.java)

            val resBody = restTemplate.postExchange(
                    "/login/log", CreateLogRequest("iOS"), testHeaders(uuid), LoginLogs::class.java).body!!

            assertThat(resBody.id).isEqualTo(userAndLogId)
            assertThat(resBody.userId).isEqualTo(userAndLogId)
            assertThat(resBody.os).isEqualTo("ios")
        }
    }

}
