package com.example.demo.integration

import com.example.demo.controller.request.TransactionEditRequest
import com.example.demo.controller.response.TransactionCreateResponse
import com.example.demo.entity.User
import com.example.demo.testutils.postExchange
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import java.time.LocalDateTime


internal class TransactionControllerTest : BaseIntegrationTest() {

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Test
    fun `Create a transaction MUST succeed`() {
        restTemplate.postExchange("/user/create", null, testHeaders(), User::class.java)

        val now = LocalDateTime.now()
        val body = TransactionEditRequest(amount = 100, categoryId = 1, accountId = 1, transactionDateTime = now)
        val res = restTemplate.postExchange("/transaction/create", body, testHeaders(), TransactionCreateResponse::class.java)

        assertThat(res.body!!.id).isEqualTo(1)
        assertThat(res.body!!.amount).isEqualTo(100)
    }


}
