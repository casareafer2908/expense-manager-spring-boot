package com.example.demo.integration

import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.test.context.ActiveProfiles
import java.util.*
import javax.sql.DataSource


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class BaseIntegrationTest {
    @Autowired
    private lateinit var dataSource: DataSource

    @BeforeEach
    fun clearH2Database() {
        val conn = dataSource.connection
        val statement = conn.createStatement()
        statement.execute("SET REFERENTIAL_INTEGRITY FALSE")
        // Find all tables and truncate them
        val tables = HashSet<String>()
        var rs = statement.executeQuery(
                "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'"
        )
        while (rs.next()) {
            tables.add(rs.getString(1))
        }
        rs.close()
        for (table in tables) {
            statement.executeUpdate("TRUNCATE TABLE $table")
        }

        // Idem for sequences
        val sequences = HashSet<String>()
        rs = statement.executeQuery(
                "SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'"
        )
        while (rs.next()) {
            sequences.add(rs.getString(1))
        }
        rs.close()
        for (seq in sequences) {
            statement.executeUpdate("ALTER SEQUENCE $seq RESTART WITH 1")
        }

        // Enable FK
        statement.execute("SET REFERENTIAL_INTEGRITY TRUE")
        statement.close()
        conn.close()
    }

    fun testHeaders(userId: String = "testUser"): HttpHeaders {
        val headers = HttpHeaders()
        headers.add("Authorization", "Bearer $userId")
        return headers
    }
}
