package com.example.demo.integration

import com.example.demo.entity.User
import com.example.demo.testutils.postExchange
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate

internal class UserControllerTest : BaseIntegrationTest() {

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Test
    fun `WHEN multiple user are created, their id MUST be incremented by one`() {
        for (userId in 1L..10L) {
            val uuid = "uuidx$userId"
            val resBody = restTemplate.postExchange(
                    "/user/create", null, testHeaders(uuid), User::class.java).body!!

            assertThat(resBody.id).isEqualTo(userId)
            assertThat(resBody.uuid).isEqualTo(uuid)
        }
    }


}
