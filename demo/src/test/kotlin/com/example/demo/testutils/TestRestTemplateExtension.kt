package com.example.demo.testutils

import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.util.MultiValueMap

fun <T> TestRestTemplate.postExchange(
        url: String,
        body: Any? = null,
        headers: MultiValueMap<String, String>? = null,
        responseType: Class<T>): ResponseEntity<T> {
    return exchange(url, HttpMethod.POST, HttpEntity(body, headers), responseType)
}

