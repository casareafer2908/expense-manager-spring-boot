package com.example.demo.unit

import com.example.demo.entity.Accounts
import com.example.demo.entity.Category
import com.example.demo.entity.Transaction
import com.example.demo.entity.User
import com.example.demo.repository.AccountsRepository
import com.example.demo.repository.CategoryRepository
import com.example.demo.repository.TransactionRepository
import com.example.demo.repository.UserRepository
import com.example.demo.security.DevelopJwtVerifierService
import com.example.demo.security.JwtVerifierDevelopConfig
import com.example.demo.security.JwtVerifierService
import com.example.demo.service.TransactionService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.time.LocalDateTime
import java.util.*

internal class JwtVerifierServiceTest {

    @Test
    fun `GIVEN a custom JTW token get the correct values`() {
        val jwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0U3ViIiwibmFtZSI6InRlc3ROYW1lIiwiYWRta" +
                "W4iOnRydWUsImp0aSI6ImQ5MGEyNmJlLWQ2NWQtNDE0YS04NDNkLWMyMTQzZDMxYjFlOCIsImlhdCI6MTU4MDU2MzM2NSwiZXhwIj" +
                "oxNTgwNTY3MDI2fQ.2lkGhJKK2ljL9_aV15PsVkOtMY7-HNiuBHkYJExdOpI"

        val service = DevelopJwtVerifierService()
        Assertions.assertThat(service.getClaim(jwtToken, "sub")).isEqualTo("testSub")
        Assertions.assertThat(service.getClaim(jwtToken, "name")).isEqualTo("testName")
    }
    
}
