package com.example.demo.unit

import com.example.demo.entity.Accounts
import com.example.demo.entity.Category
import com.example.demo.entity.Transaction
import com.example.demo.entity.User
import com.example.demo.repository.AccountsRepository
import com.example.demo.repository.CategoryRepository
import com.example.demo.repository.TransactionRepository
import com.example.demo.repository.UserRepository
import com.example.demo.service.TransactionService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.time.LocalDateTime
import java.util.*

internal class TransactionServiceTests {
    private val service = TransactionService(
            Mockito.mock(TransactionRepository::class.java),
            Mockito.mock(CategoryRepository::class.java),
            Mockito.mock(AccountsRepository::class.java),
            Mockito.mock(UserRepository::class.java)
    )

    @Test
    fun `GIVEN some transactions the total amount is correct`() {
        val transactions = arrayListOf(
                createTransaction(amount = 5),
                createTransaction(amount = 15),
                createTransaction(amount = -15)
        )
        val result = service.transactionGetByUserResponse(transactions)

        Assertions.assertThat(result.totalAmount).isEqualTo(5)
        Assertions.assertThat(result.totalTransactions).isEqualTo(3)
        Assertions.assertThat(result.transactions.size).isEqualTo(3)
    }

    private fun createTransaction(
            id: Long = 0,
            amount: Long,
            userId: Long = 0,
            creationDate: LocalDateTime = LocalDateTime.now(),
            latestUpdate: LocalDateTime = LocalDateTime.now()): Transaction {
        val user = User(1, UUID.randomUUID().toString(), LocalDateTime.now())
        val category = Category(1, "category 1", "Description", "https://xxx", 1)
        val account = Accounts(1, "category 1", "Description", 100, "https://xxx", 1)
        return Transaction(id, amount, "Description", LocalDateTime.now(), category, account, user, creationDate, latestUpdate)
    }

}
