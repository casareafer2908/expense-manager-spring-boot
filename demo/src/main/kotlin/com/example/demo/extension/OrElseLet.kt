package com.example.demo.extension

/**
 * <pre>
 *  var dog:String? = null;
 *
 *  dog?.let {
 *      println("do something when NOT NULL");
 *  }.orElse {
 *      println("do something when NULL");
 *  }</pre>
 */
inline fun <R> R?.orElse(block: () -> R): R {
    return this ?: block()
}
