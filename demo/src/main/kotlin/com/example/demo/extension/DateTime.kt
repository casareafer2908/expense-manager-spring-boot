package com.example.demo.extension

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

fun LocalDate.atEndOfDay(): LocalDateTime {
    return this.atTime(LocalTime.MAX)
}

