package com.example.demo.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "login_logs")
data class LoginLogs(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        var id: Long? = null,

        //Android or iOS
        @Column(name = "os")
        val os: String? = null,

        @Column(name = "latest_login")
        var latestLogin: LocalDateTime,

        @Column(name = "user_id")
        val userId: Long
)

