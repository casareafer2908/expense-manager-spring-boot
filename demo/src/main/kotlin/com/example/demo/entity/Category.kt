package com.example.demo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "categories")
data class Category(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long? = null,

        @get: NotBlank
        @Column(name = "name")
        val name: String,

        @Column(name = "description")
        val description: String? = null,

        @Column(name = "icon_url")
        val iconUrl: String? = null,

        @Column(name = "user_id")
        val userId: Long
)

