package com.example.demo.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "transactions")
data class Transaction(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long? = null,

        @Column(name = "amount")
        var amount: Long = 0,

        @Column(name = "description")
        var description: String? = null,

        @Column(name = "transaction_date")
        var transactionDateTime: LocalDateTime,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "category_id")
        var category: Category,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "account_id")
        var account: Accounts,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        val user: User,

        @Column(name = "creation_date")
        val creationDate: LocalDateTime,

        @Column(name = "latest_update")
        var latestUpdate: LocalDateTime
)

