package com.example.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class KotlinDemoApplication

const val PROFILE_DEVELOP = "dev"
const val PROFILE_PRODUCTION = "prod"
const val PROFILE_TEST = "test"

/*
    To run:
        Dev:    ./gradlew bootRun --args='--spring.profiles.active=dev'
        Prod:   ./gradlew bootRun --args='--spring.profiles.active=prod'
        Test:   ./gradlew clean test info
 */

fun main(args: Array<String>) {
    SpringApplication.run(KotlinDemoApplication::class.java, *args)
}
