package com.example.demo.repository

import com.example.demo.entity.Transaction
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface TransactionRepository : JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * FROM transactions " +
            "WHERE user_id = :userId AND transaction_date BETWEEN :startDate AND :endDate " +
            "ORDER BY transaction_date,id ASC", nativeQuery = true)
    fun get(
            @Param("startDate") startDate: LocalDateTime,
            @Param("endDate") endDate: LocalDateTime,
            @Param("userId") userId: Long): List<Transaction>
}
