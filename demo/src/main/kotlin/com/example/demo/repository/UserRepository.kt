package com.example.demo.repository

import com.example.demo.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import javax.transaction.Transactional

@Repository
interface UserRepository : JpaRepository<User, Long> {

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "UPDATE users SET latest_login = :date WHERE id=:userId")
    fun updateLatestLoginDate(
            @Param("userId") userId: Long,
            @Param("date") date: LocalDateTime)

    @Query(nativeQuery = true, value = "SELECT * FROM users WHERE uuid=:uuid")
    fun findByUUID(@Param("uuid") uuid: String): User?
}
