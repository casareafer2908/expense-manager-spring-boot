package com.example.demo.repository

import com.example.demo.entity.Accounts
import com.example.demo.entity.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface AccountsRepository : JpaRepository<Accounts, Long> {

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "DELETE FROM accounts WHERE id=:id")
    fun delete(@Param("id") id: Long): Int

}
