package com.example.demo.repository

import com.example.demo.entity.LoginLogs
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LoginLogsRepository : JpaRepository<LoginLogs, Long>
