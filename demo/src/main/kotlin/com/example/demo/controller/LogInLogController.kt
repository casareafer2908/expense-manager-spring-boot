package com.example.demo.controller

import com.example.demo.controller.request.CreateLogRequest
import com.example.demo.controller.utils.isEqualsTo
import com.example.demo.entity.LoginLogs
import com.example.demo.security.CustomAuthenticationToken
import com.example.demo.service.LoginLogsService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.IllegalArgumentException

@RestController
@RequestMapping("/login")
class LogInLogController(
        private val loginLogsService: LoginLogsService
) {

    @PostMapping("/log")
    fun createLog(user: CustomAuthenticationToken, @RequestBody request: CreateLogRequest): LoginLogs {
        val os = request.os.toLowerCase()
        isEqualsTo(os, arrayOf("android", "ios", "web")) { "os must be Android, iOS or WEB" }
        return loginLogsService.createLatestLoginLog(user.id, os)
    }
}
