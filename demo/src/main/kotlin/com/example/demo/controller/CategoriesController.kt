package com.example.demo.controller

import com.example.demo.controller.request.CategoryRequest
import com.example.demo.entity.Category
import com.example.demo.security.CustomAuthenticationToken
import com.example.demo.service.CategoriesService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/categories")
class CategoriesController(
        private val categoriesService: CategoriesService
) {

    @PostMapping("/getAll")
    fun getAll(user: CustomAuthenticationToken): List<Category> = categoriesService.getAll(user.id)

    @PostMapping("/create")
    fun createCategory(user: CustomAuthenticationToken, @RequestBody request: CategoryRequest):
            Category = categoriesService.createCategory(user.id, request)

    @PostMapping("/update")
    fun updateCategory(user: CustomAuthenticationToken, @RequestBody request: CategoryRequest):
            Category = categoriesService.updateCategory(user.id, request)

    @PostMapping("/delete")
    fun deleteCategory(user: CustomAuthenticationToken, @RequestBody request: CategoryRequest):
            String = categoriesService.deleteCategory(user.id, request)

}
