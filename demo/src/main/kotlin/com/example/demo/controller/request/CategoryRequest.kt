package com.example.demo.controller.request

data class CategoryRequest(
        val id: Long?,
        val name: String,
        val iconUrl: String?
)

