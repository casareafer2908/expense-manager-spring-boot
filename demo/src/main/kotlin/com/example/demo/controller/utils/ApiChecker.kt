package com.example.demo.controller.utils

import java.time.LocalDate

fun isNull(value: Any?, message: () -> Any) {
    if (value == null) {
        throw IllegalArgumentException(message().toString())
    }
}

fun startDateIsBeforeEndDate(startDate: LocalDate, endDate: LocalDate, message: () -> Any) {
    if (startDate.isAfter(endDate)) {
        throw IllegalArgumentException(message().toString())
    }
}

fun isEqualsTo(text: String, arrToCompare: Array<String>, message: () -> Any) {
    for (auxToCompare in arrToCompare) {
        if (text == auxToCompare) {
            return
        }
    }
    throw IllegalArgumentException(message().toString())
}
