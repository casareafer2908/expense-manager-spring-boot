package com.example.demo.controller.request

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDateTime

data class TransactionEditRequest(
        val id: Long? = null,
        val amount: Long,
        val description: String? = null,
        val categoryId: Long,
        val accountId: Long,

        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        val transactionDateTime: LocalDateTime
)
