package com.example.demo.controller.response

data class TransactionGetByUserResponse(
        val totalAmount: Long,
        val totalTransactions: Long,
        val transactions: List<TransactionCreateResponse>
)
