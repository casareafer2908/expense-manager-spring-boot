package com.example.demo.controller.response

import java.time.LocalDateTime

data class TransactionCreateResponse(
        val id: Long,
        val amount: Long,
        val categoryName: String,
        val accountName: String,
        val transactionDateTime: LocalDateTime
)
