package com.example.demo.controller.request

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate

data class TransactionGetByUserRequest(
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        val startDate: LocalDate,

        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        val endDate: LocalDate
) {
    override fun toString(): String {
        return "startDate: $startDate, endDate: $endDate"
    }
}
