package com.example.demo.controller

import com.example.demo.entity.User
import com.example.demo.security.CustomAuthenticationToken
import com.example.demo.security.JwtVerifierService
import com.example.demo.service.UserService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/user")
class UserController(
        private val userService: UserService,
        private val jwtVerifierService: JwtVerifierService
) {

    @PostMapping("/create")
    fun createUser(@RequestHeader("Authorization") authorization: String): User {
        val authToken = authorization.removePrefix("Bearer ")
        jwtVerifierService.verify(authToken)
        val uuid = jwtVerifierService.getClaim(authToken, "sub")
        return userService.createUser(uuid)
    }

    @PostMapping("/login")
    fun loginUser(user: CustomAuthenticationToken): User = userService.loginUser(user.id)
}

