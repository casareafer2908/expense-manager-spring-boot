package com.example.demo.controller.response

import java.time.LocalDateTime

data class TransactionUpdateResponse(
        val id: Long,
        val amount: Long,
        val categoryName: String,
        val accountName: String,
        val description: String?,
        val transactionDateTime: LocalDateTime,
        val latestUpdate: LocalDateTime
)
