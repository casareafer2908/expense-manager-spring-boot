package com.example.demo.controller

import com.example.demo.controller.request.TransactionEditRequest
import com.example.demo.controller.request.TransactionGetByUserRequest
import com.example.demo.controller.response.TransactionCreateResponse
import com.example.demo.controller.response.TransactionGetByUserResponse
import com.example.demo.controller.response.TransactionUpdateResponse
import com.example.demo.controller.utils.isNull
import com.example.demo.controller.utils.startDateIsBeforeEndDate
import com.example.demo.security.CustomAuthenticationToken
import com.example.demo.service.TransactionService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transaction")
class TransactionController(
        private val transactionService: TransactionService
) {
    @PostMapping("/create")
    fun create(user: CustomAuthenticationToken, @RequestBody request: TransactionEditRequest): TransactionCreateResponse {
        return transactionService.createTransaction(user.id, request)
    }

    @PostMapping("/get")
    fun loginUser(user: CustomAuthenticationToken, @RequestBody request: TransactionGetByUserRequest): TransactionGetByUserResponse {
        startDateIsBeforeEndDate(request.startDate, request.endDate) { "Start date must be a date before end date" }
        return transactionService.getAllUserTransactions(user.id, request)
    }

    @PostMapping("/update")
    fun update(user: CustomAuthenticationToken, @RequestBody request: TransactionEditRequest): TransactionUpdateResponse {
        isNull(request.id) { "missing id" }
        return transactionService.updateTransaction(user.id, request)
    }
}
