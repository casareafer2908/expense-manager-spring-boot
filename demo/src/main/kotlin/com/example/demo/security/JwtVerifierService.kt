package com.example.demo.security

import com.auth0.jwk.InvalidPublicKeyException
import com.auth0.jwk.UrlJwkProvider
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTDecodeException
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import com.google.firebase.auth.FirebaseAuth
import org.springframework.stereotype.Service
import java.net.URL
import java.security.interfaces.RSAPublicKey


@Service
interface JwtVerifierService {

    /**
     * @return: if the token verification fail will throw an exception
     */
    fun verify(token: String)

    fun getClaim(token: String, claimName: String): String
}

class AWSCognitoJwtVerifierService : JwtVerifierService {
    override fun verify(token: String) {
        val url = "https://cognito-idp.ap-northeast-1.amazonaws.com/ap-northeast-1_yPg9a7XQJ/.well-known/jwks.json"
        val jwkProvider = UrlJwkProvider(URL(url))

        val jwt = JWT.decode(token)
        val jwk = jwkProvider.get(jwt.keyId)

        val publicKey = jwk.publicKey as? RSAPublicKey ?: throw JWTVerificationException("Invalid key type") // safe

        val algorithm = when (jwk.algorithm) {
            "RS256" -> Algorithm.RSA256(publicKey, null)
            else -> throw JWTVerificationException("Unsupported algorithm")
        }

        val verifier = JWT.require(algorithm) // signature
                .build()

        verifier.verify(token)
    }

    override fun getClaim(token: String, claimName: String): String {
        val jwt = JWT.decode(token)
        return jwt.getClaim(claimName).asString()
    }
}

class DevelopJwtVerifierService : JwtVerifierService {
    override fun verify(token: String) {
        getToken(token)
    }

    override fun getClaim(token: String, claimName: String): String {
        return getToken(token).getClaim(claimName).asString()
    }

    private fun getToken(token: String): DecodedJWT {
        return try {
            JWT.decode(token)
        } catch (e: JWTDecodeException) {
            JWT.decode(JWT
                    .create()
                    .withClaim("sub", token)
                    .sign(Algorithm.none()))
        }
    }
}

class FirebaseJwtVerifierService : JwtVerifierService {
    override fun verify(token: String) {
        FirebaseAuth.getInstance().verifyIdToken(token, true)
    }

    override fun getClaim(token: String, claimName: String): String {
        return FirebaseAuth.getInstance().verifyIdToken(token).uid
    }
}
