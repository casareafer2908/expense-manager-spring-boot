package com.example.demo.security

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class CustomAuthenticationToken(val id: Long, val uuid: String, val token: String,
                                authorities: Collection<GrantedAuthority>) : AbstractAuthenticationToken(authorities) {

    override fun getCredentials(): Any? = null
    override fun getPrincipal(): Any? = id
    override fun isAuthenticated(): Boolean = true
    override fun toString(): String = "id: $id, uuid:$uuid, token:$token"

}
