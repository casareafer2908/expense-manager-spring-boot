package com.example.demo.security

import com.example.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class WebSecurityConfig(
        private var unauthorizedHandler: JwtAuthenticationEntryPoint,
        private var jwtVerifierService: JwtVerifierService,
        private var userRepository: UserRepository
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun authenticationTokenFilterBean():
            JtwAuthenticationFilter = JtwAuthenticationFilter(userRepository, jwtVerifierService)

    @Throws(Exception::class)
    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity
                // we don't need CSRF because our token is invulnerable
                .csrf().disable()
                .authorizeRequests()
                // create user DO NOT need to be authenticated
                .antMatchers("/user/create").permitAll()
                // All urls must be authenticated (filter for token always fires (/**)
                .anyRequest().authenticated()
                .and()
                // Call our errorHandler if authentication/authorisation fails
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
                .and()
                // Don't create session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // Custom JWT based security filter
        httpSecurity
                .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter::class.java)

        // disable page caching
        httpSecurity.headers().cacheControl()
    }
}
