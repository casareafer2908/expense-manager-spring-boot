package com.example.demo.security

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.util.ResourceUtils
import java.io.FileInputStream


private const val VERIFY_TOKEN_STRATEGY_COGNITO = "COGNITO"
private const val VERIFY_TOKEN_STRATEGY_FIREBASE = "FIREBASE"
private const val VERIFY_TOKEN_STRATEGY_DEVELOP = "DEVELOP"

@Configuration
@ConditionalOnProperty(name = ["com.verify_token.strategy"], havingValue = VERIFY_TOKEN_STRATEGY_COGNITO)
class JwtVerifierCognitoConfig {
    @Bean
    fun jwtVerifier(): JwtVerifierService = AWSCognitoJwtVerifierService()
}

@Configuration
@ConditionalOnProperty(name = ["com.verify_token.strategy"], havingValue = VERIFY_TOKEN_STRATEGY_FIREBASE)
class JwtVerifierFirebaseConfig {
    @Bean
    fun jwtVerifier(): JwtVerifierService {
        val fileServiceAccount = ResourceUtils.getFile("classpath:firebase/serviceAccountFirebase.json")
        val inputStreamServiceAccount = FileInputStream(fileServiceAccount)
        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(inputStreamServiceAccount))
                .setDatabaseUrl("https://spicypepper-10b10b.firebaseio.com")
                .build()
        FirebaseApp.initializeApp(options)
        return FirebaseJwtVerifierService()
    }
}

@Configuration
@ConditionalOnProperty(name = ["com.verify_token.strategy"], havingValue = VERIFY_TOKEN_STRATEGY_DEVELOP)
class JwtVerifierDevelopConfig {
    @Bean
    fun jwtVerifier(): JwtVerifierService = DevelopJwtVerifierService()
}

