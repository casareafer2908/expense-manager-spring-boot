package com.example.demo.security

import com.auth0.jwt.exceptions.JWTVerificationException
import com.example.demo.entity.User
import com.example.demo.repository.UserRepository
import org.slf4j.LoggerFactory
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JtwAuthenticationFilter(
        private val userRepository: UserRepository,
        private val jwtVerifierService: JwtVerifierService
) : OncePerRequestFilter() {
    private val log = LoggerFactory.getLogger(JtwAuthenticationFilter::class.java)


    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, chain: FilterChain) {
        if (!req.requestURL.toString().contains("user/create")) {
            val tokenHeader = req.getHeader("Authorization")

            if (tokenHeader.isNullOrBlank() || !tokenHeader.startsWith("Bearer ")) {
                log.warn("JWT Token does not begin with Bearer String")
            } else try {
                val authToken = tokenHeader.removePrefix("Bearer ")
                jwtVerifierService.verify(authToken)
                val uuid = jwtVerifierService.getClaim(authToken, "sub")
                val user = loadUserByUsername(uuid)
                val authUser = CustomAuthenticationToken(user.id!!, uuid, authToken, arrayListOf())
                SecurityContextHolder.getContext().authentication = authUser
            } catch (e: JWTVerificationException) {
                log.info(e.message)
            }
        }
        chain.doFilter(req, res)
    }


    private fun loadUserByUsername(uuid: String): User {
        return userRepository.findByUUID(uuid) ?: throw UsernameNotFoundException("User not found from token provided.")
    }
}


