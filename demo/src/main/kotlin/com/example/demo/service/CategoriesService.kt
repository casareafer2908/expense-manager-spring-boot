package com.example.demo.service

import com.example.demo.controller.request.CategoryRequest
import com.example.demo.entity.Category
import com.example.demo.repository.CategoryRepository
import com.example.demo.repository.UserRepository
import org.springframework.stereotype.Repository


@Repository
class CategoriesService(
        private val categoryRepository: CategoryRepository,
        private val userRepository: UserRepository
) {

    fun getAll(userId: Long): List<Category> {
        return categoryRepository.findAll()
    }

    fun createCategory(userId: Long, request: CategoryRequest): Category {
        val cat = Category(name = request.name, iconUrl = request.iconUrl, userId = 1)//TODO USER ID CANNOT BE HARCODED
        return categoryRepository.save(cat)
    }

    fun updateCategory(userId: Long, request: CategoryRequest): Category {
        val cat = Category(id = request.id, name = request.name, iconUrl = request.iconUrl, userId = 1)//TODO USER ID CANNOT BE HARCODED
        return categoryRepository.save(cat)
    }

    fun deleteCategory(userId: Long, request: CategoryRequest): String {
        var success = 0
        request.id?.let {
            success = categoryRepository.delete(it)
        }
        return if (success == 1) "Delete successfully" else "Cannot delete category ${request.name}"
    }
}
