package com.example.demo.service

import com.example.demo.entity.Accounts
import com.example.demo.entity.Category
import com.example.demo.entity.User
import com.example.demo.repository.AccountsRepository
import com.example.demo.repository.CategoryRepository
import com.example.demo.repository.UserRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class UserService(
        private val userRepository: UserRepository,
        private val categoryRepository: CategoryRepository,
        private val accountsRepository: AccountsRepository
) {
    fun createUser(uuid: String): User {
        userRepository.findByUUID(uuid)?.let {
            return it
        }
        val now = LocalDateTime.now()
        var user = User(null, uuid, now)
        user = userRepository.save(user)
        initializeUserDefaults(user)
        return user
    }

    fun initializeUserDefaults(user: User) {
        val userId = user.id!!
        val categories = arrayListOf(
                Category(name = "Salary", userId = userId),
                Category(name = "Car", userId = userId),
                Category(name = "Parking Lot", userId = userId),
                Category(name = "Water", userId = userId),
                Category(name = "Internet", userId = userId),
                Category(name = "Electricity", userId = userId),
                Category(name = "Gas", userId = userId),
                Category(name = "House bills", userId = userId)
        )
        for (auxCategory in categories) {
            categoryRepository.save(auxCategory)
        }
        val accounts = arrayListOf(
                Accounts(name = "Bank", userId = userId),
                Accounts(name = "Credit Card", userId = userId),
                Accounts(name = "Cash", userId = userId),
                Accounts(name = "Saving", userId = userId),
                Accounts(name = "Others", userId = userId)
        )
        for (auxAccounts in accounts) {
            accountsRepository.save(auxAccounts)
        }
    }


    fun loginUser(userId: Long): User {
        userRepository.updateLatestLoginDate(1, LocalDateTime.now())
        return userRepository.findById(1).get()
    }

}
