package com.example.demo.service

import com.example.demo.controller.request.TransactionEditRequest
import com.example.demo.controller.request.TransactionGetByUserRequest
import com.example.demo.controller.response.TransactionCreateResponse
import com.example.demo.controller.response.TransactionGetByUserResponse
import com.example.demo.controller.response.TransactionUpdateResponse
import com.example.demo.entity.Transaction
import com.example.demo.extension.atEndOfDay
import com.example.demo.repository.AccountsRepository
import com.example.demo.repository.CategoryRepository
import com.example.demo.repository.TransactionRepository
import com.example.demo.repository.UserRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime


@Repository
class TransactionService(
        private val transactionRepository: TransactionRepository,
        private val categoryRepository: CategoryRepository,
        private val accountsRepository: AccountsRepository,
        private val userRepository: UserRepository
) {

    fun getAllUserTransactions(userId: Long, request: TransactionGetByUserRequest): TransactionGetByUserResponse {
        // println("Query $request")
        val startDate = request.startDate.atStartOfDay()
        val endDate = request.endDate.atEndOfDay()
        return transactionGetByUserResponse(transactionRepository.get(startDate, endDate, userId))
    }

    fun createTransaction(userId: Long, request: TransactionEditRequest): TransactionCreateResponse {
        val category = categoryRepository.getOne(request.categoryId)
        val account = accountsRepository.getOne(request.accountId)
        val user = userRepository.getOne(userId)
        val now = LocalDateTime.now()
        var transaction = Transaction(
                amount = request.amount,
                category = category,
                account = account,
                description = request.description,
                user = user,
                creationDate = now,
                latestUpdate = now,
                transactionDateTime = request.transactionDateTime
        )

        transaction = transactionRepository.save(transaction)
        return TransactionCreateResponse(
                transaction.id!!,
                transaction.amount,
                transaction.category.name,
                transaction.account.name,
                transaction.transactionDateTime
        )
    }

    fun updateTransaction(userId: Long, request: TransactionEditRequest): TransactionUpdateResponse {
        val category = categoryRepository.getOne(request.categoryId)
        val account = accountsRepository.getOne(request.accountId)
        val now = LocalDateTime.now()
        var transactionToUpdate = transactionRepository.getOne(request.id!!)

        transactionToUpdate.amount = request.amount
        transactionToUpdate.description = request.description
        transactionToUpdate.category = category
        transactionToUpdate.account = account
        transactionToUpdate.latestUpdate = now
        transactionToUpdate.transactionDateTime = request.transactionDateTime

        transactionToUpdate = transactionRepository.save(transactionToUpdate)
        return TransactionUpdateResponse(
                id = transactionToUpdate.id!!,
                amount = transactionToUpdate.amount,
                categoryName = transactionToUpdate.category.name,
                accountName = transactionToUpdate.account.name,
                description = transactionToUpdate.description,
                transactionDateTime = transactionToUpdate.transactionDateTime,
                latestUpdate = transactionToUpdate.latestUpdate
        )
    }

    fun transactionGetByUserResponse(transactions: List<Transaction>): TransactionGetByUserResponse {
        var total = 0L
        val transactionRes = transactions.map {
            total += it.amount

            TransactionCreateResponse(
                    id = it.id!!,
                    amount = it.amount,
                    accountName = it.account.name,
                    categoryName = it.category.name,
                    transactionDateTime = it.transactionDateTime)
        }.toList()

        return TransactionGetByUserResponse(
                totalAmount = total,
                totalTransactions = transactions.size.toLong(),
                transactions = transactionRes)
    }
}
