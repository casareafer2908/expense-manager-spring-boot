package com.example.demo.service


import com.example.demo.entity.LoginLogs
import com.example.demo.repository.LoginLogsRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime


@Repository
class LoginLogsService(
        private val loginLogsRepository: LoginLogsRepository
) {

    fun createLatestLoginLog(userId: Long, os: String): LoginLogs {
        val now = LocalDateTime.now()
        var logEntry = LoginLogs(null, os, now, userId)
        logEntry = loginLogsRepository.save(logEntry)
        return logEntry
    }

}
