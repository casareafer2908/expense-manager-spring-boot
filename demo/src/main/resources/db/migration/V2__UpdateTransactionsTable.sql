ALTER TABLE transactions MODIFY creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
