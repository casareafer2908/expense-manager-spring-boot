import 'package:expense_manager/services/api.dart';
import 'package:flutter/material.dart';

class MainMenuScreen extends StatefulWidget {
  MainMenuScreen({@required this.apiBase});

  final ApiBase apiBase;

  @override
  _MainMenuScreenState createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends State<MainMenuScreen> {
  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Expense Manager'),
        elevation: 2.0,
      ),
      body: Text("Que pedorron"),
    );
  }

  Future<void> initData() async {
    await widget.apiBase.createUser();
    await widget.apiBase.updateLogin();
  }
}
