import 'dart:async';

import 'package:expense_manager/services/auth.dart';
import 'package:flutter/foundation.dart';

class LoginManager {
  LoginManager({@required this.auth, @required this.isLoading});

  final AuthBase auth;
  final ValueNotifier<bool> isLoading;

  Future<AuthUser> _signIn(Future<AuthUser> Function() signInMethod) async {
    try {
      isLoading.value = true;
      return await signInMethod();
    } catch (e) {
      isLoading.value = false;
      rethrow;
    }
  }

  Future<AuthUser> signInWithGoogle() async => await _signIn(auth.signInWithGoogle);
}
