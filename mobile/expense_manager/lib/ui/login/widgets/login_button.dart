import 'package:expense_manager/ui/widgets/custom_raised_button.dart';
import 'package:flutter/material.dart';

class LoginButton extends CustomRaisedButton {
  LoginButton({
    Key key,
    @required String text,
    Color color,
    Color textColor,
    VoidCallback onPressed,
  }) : assert(text != null),
        super(
          key: key,
          child: Text(
            text,
            style: TextStyle(color: textColor, fontSize: 15.0),
          ),
          color: color,
          onPressed: onPressed,
        );
}
