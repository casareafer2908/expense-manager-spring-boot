import 'package:expense_manager/constants/dimensions.dart';
import 'package:expense_manager/services/auth.dart';
import 'package:expense_manager/ui/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen(this.auth);

  final AuthBase auth;

  static Widget create(BuildContext context) {
    final auth = Provider.of<AuthBase>(context, listen: false);
    return SplashScreen(auth);
  }

  @override
  _SplashScreenState createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _initialize();
  }

  void _initialize() async {
    final user = await widget.auth.currentUser();
    if (user == null) {
      _continueToLogin();
    } else {
      _continueToMainMenu(user);
    }
  }

  _continueToLogin() {
    Navigator.pushNamedAndRemoveUntil(context, Routes.login, (r) => false);
  }

  _continueToMainMenu(AuthUser user) {
    Navigator.pushNamedAndRemoveUntil(context, Routes.mainMenu, (r) => false, arguments: user);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(height: dim16),
            Text('Loading...'),
          ],
        ),
      ),
    );
  }
}
