import 'package:expense_manager/services/auth.dart';
import 'package:expense_manager/ui/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<AuthBase>(
      create: (BuildContext providerContext) => Auth(),
      child: MaterialApp(
        title: 'Expense Manager',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        onGenerateRoute: (RouteSettings settings) => Routes.routes(settings),
      ),
    );
  }
}
