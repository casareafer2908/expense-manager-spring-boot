import 'package:expense_manager/services/api.dart';
import 'package:expense_manager/services/auth.dart';
import 'package:expense_manager/ui/splash_screen.dart';
import 'package:flutter/material.dart';

import 'login/login_screen.dart';
import 'main_menu/main_menu.dart';

class Routes {
  static final splash = "/";
  static final login = "/login";
  static final mainMenu = "/main_menu";

  static Route routes(RouteSettings settings) {
    //Splash screen
    if (settings.name == splash) {
      return MaterialPageRoute(
        builder: (context) {
          return SplashScreen.create(context);
        },
      );
    } else if (settings.name == login) {
      return MaterialPageRoute(
        builder: (context) {
          return LoginScreen.create(context);
        },
      );
    } else if (settings.name == mainMenu) {
      final AuthUser user = settings.arguments;
      return _getMaterialPageRoute(MainMenuScreen(
        apiBase: Api(token: user.token),
      ));
    }

    return null;
  }

  static MaterialPageRoute _getMaterialPageRoute(Widget widget) {
    return MaterialPageRoute(
      builder: (context) {
        return widget;
      },
    );
  }
}
