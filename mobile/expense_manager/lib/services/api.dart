import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;

abstract class ApiBase {
  Future<void> createUser();

  Future<void> updateLogin();
}

class Api implements ApiBase {
  Api({@required this.token}) : assert(token != null);

  final String root = 'http://10.0.2.2:8080'; //AVD uses 10.0.2.2 as an alias to LOCALHOST
  final String token;

  Client client = Client();

  @override
  Future<void> createUser() async {
    await client.post('$root/user/create', headers: getHeaders());
  }

  @override
  Future<void> updateLogin() async {
    final body = jsonEncode({"os": "Android"});
    await client.post('$root/login/log', headers: getHeaders(), body: body);
  }

  Map<String, String> getHeaders() {
    return {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token",
    };
  }
}
